package com.pt.pedrorijo91.scala99problems

import scala.util.Random
import scala.collection.mutable.ListBuffer

object Scala99Lists {

  private def last[T](lst: List[T]): T =
    lst match {
    case Nil => throw new java.util.NoSuchElementException()
    case x :: Nil => x
    case _ :: xs => last(xs) 
  }
  
  def p01[T](lst: List[T]) = last(lst)
  
  private def penultimate[T](lst: List[T]): T =
    lst match {
    case x :: _ :: Nil => x
    case _ :: tail => penultimate(tail)
    case _ => throw new java.util.NoSuchElementException()
  }
  
  def p02[T](lst: List[T]) = penultimate(lst)
  
  private def nth[T](n: Int, lst: List[T]): T =
    (n,lst) match {
    case (0, lst) => lst.head
    case (n, lst) => nth(n-1, lst.tail)
  }
  
  def p03[T](n: Int, lst: List[T]) = nth(n, lst)
  
  private def length[T](lst: List[T]): Int = 
    lst match {
    case Nil => 0
    case _ :: tail => 1 + length(tail)
  }
  
  def p04[T](lst: List[T]) = length(lst)

  private def reverse[T](lst: List[T]): List[T] =
    lst match {
    case Nil => Nil
    case head :: tail => reverse(tail) :+ head
  }
  
  def p05[T](lst: List[T]) = reverse(lst)

  private def isPalindrome[T](lst: List[T]): Boolean =
    if (lst.isEmpty || lst.length == 1)
    	true
    else
      lst(0) == lst(lst.length -1) && isPalindrome(lst.slice(1, lst.length-1))

  def p06[T](lst: List[T]) = isPalindrome(lst)
  
  private def flatten[T](lst: List[T]): List[T] = 
    lst.flatMap(
      x => x match {
      case el: List[T] => flatten(el)
      case el => List(el)
      })
  
  def p07[T](lst: List[T]) = flatten(lst)
      
 private def compressR[T](lst: List[T]): List[T] =
    lst.foldRight(List[T]()) ((elem, acc) =>
      if (!acc.isEmpty && acc.head == elem) acc
      else elem :: acc)
  
  private def compress[T](lst: List[T]): List[T] =
    lst.foldLeft(List[T]()) ((acc, elem) =>
      if (!acc.isEmpty && acc.last == elem) acc
      else acc :+ elem)    
      
  def p08[T](lst: List[T]) = compress(lst)
  
  private def pack[T](lst: List[T]): List[List[T]] = 
    if (lst.isEmpty) Nil
    else {
      val (equal, rest) = lst.span (_ == lst.head)
      equal :: pack(rest)
    }
  
  def p09[T](lst: List[T]) = pack(lst)
  
  private def encode[T](lst: List[T]) = pack(lst).map(x => (x.size, x.head))
  
  def p10[T](lst: List[T]) = encode(lst)
  
  private def encodeModified[T](lst: List[T]) = pack(lst).map(x => if (x.size > 1) (x.size, x.head) else x.head)
  
  def p11[T](lst: List[T]) = encodeModified(lst)
  
  private def decode[T](lst: List[(Int, T)]) = lst flatMap { elem => List.fill(elem._1)(elem._2) }
  
  def p12[T](lst: List[(Int, T)]) = decode(lst)
  
  private def encodeDirect[T](lst: List[T]): List[(Int, T)] = 
     if (lst.isEmpty) Nil
    else {
      val (equal, rest) = lst.span (_ == lst.head)
      (equal.length, equal.head) :: encodeDirect(rest)
    }
  
  def p13[T](lst: List[T]) = encodeDirect(lst)
  
  private def duplicate[T](lst: List[T]) = lst.flatMap(x => List(x, x))
  
  def p14[T](lst: List[T]) = duplicate(lst)
  
  private def duplicateN[T](n: Int, lst: List[T]) = lst.flatMap(x => List.fill(n)(x))
  
  def p15[T](n: Int, lst: List[T]) = duplicateN(n, lst)
  
  private def drop[T](n: Int, lst: List[T]) = lst.slice(0,n-1) ::: lst.slice(n, lst.length)
  
  def p16[T](n: Int, lst: List[T]) = drop(n, lst)
  
  private def split[T](n: Int, lst: List[T]) = (lst.take(n), lst.drop(n))
  
  def p17[T](n: Int, lst: List[T]) = split(n, lst)
  
  private def slice[T](start: Int, end: Int, lst: List[T]) = lst.drop(start).dropRight(lst.length - end)
  
  def p18[T](start: Int, end: Int, lst: List[T]) = slice(start, end, lst)
  
  private def rotate[T](n: Int, lst: List[T]) = {
    val rotation = ((n % lst.length) + lst.length) % lst.length
    lst.drop(rotation) ::: lst.take(rotation)
  }
  
  def p19[T](n: Int, lst: List[T]) = rotate(n, lst)
  
  private def removeAt[T](n: Int, lst: List[T]): (List[T], T) = {
    if (n == 0) (lst.tail, lst.head)
    else {
      val (l, el) = removeAt(n-1, lst.tail)
      (lst.head :: l, el)
    }
  }
  	
  def p20[T](n: Int, lst: List[T]) = removeAt(n, lst)

  private def insertAt[T](x: T, index: Int, lst: List[T]): List[T] = 
  	if (index == 0) x :: lst
  	else lst.head :: insertAt(x, index-1, lst.tail)
  
  def p21[T](x: Any, index: Int, lst: List[T]) = insertAt(x, index, lst)
  
  private def range(start: Int, end: Int): List[Int] = 
    if (start == end) List(end)
    else start :: range(start+1, end)
    
 def p22(start: Int, end: Int) = range(start, end)
 
 private def randomSelect[T](n: Int, lst: List[T]) = Random.shuffle(lst).take(n)

 def p23[T](n: Int, lst: List[T]) = randomSelect(n, lst)

 private def lotto(n: Int, r: Int) = randomSelect(n, range(1,r))
 
 def p24(n: Int, r: Int) = lotto(n, r)
 
 private def randomPermute[T](lst: List[T]) = Random.shuffle(lst)
 
 def p25[T](lst: List[T]) = randomPermute(lst)
 
 private def combinations[T](n: Int, lst: List[T]) = {
      val it = lst.combinations(n)
      var ans = List(it.next())
      
      while(it.hasNext) {
    	  ans = ans :+ (it.next())  
      }
      ans
    }
 
 def p26[T](n: Int, lst: List[T]) = combinations(n, lst)
 
 def p27() = throw new NotImplementedError()
 
 private def lsort[T](lst: List[List[T]]) = lst.sortWith((x: List[T], y: List[T]) => x.length < y.length)
 
 def p28a[T](lst: List[List[T]]) = lsort(lst)
 
 private def lsortFreq[T](lst: List[List[T]]) = 
   lst.sortWith((x: List[T], y: List[T]) => 
     lst.filter((l: List[T]) => 
       l.length == x.length).length < lst.filter((l: List[T]) => 
       l.length == y.length).length)
 
 def p28b[T](lst: List[List[T]]) = lsortFreq(lst)
 
}